use std::time::SystemTime;
use crate::rules::Rules;
use std::{time, thread};
use std::io::{stdout, Write};
use crossterm::{
    ExecutableCommand, QueueableCommand,
    terminal, cursor, style::{self, Stylize}, Result
};
use rand::Rng;

pub struct Game {
    board: Vec<Vec<Cell>>,
    rules: Box<dyn Rules>,
}

impl Game {
    pub fn new(x: u16, y: u16, rules: Box<dyn Rules>) -> Game {
        let mut board = Vec::new();

        for _ in 0..y {
            let mut row = Vec::new();

            for _ in 0..x {
                row.push(Cell::Dead);
            }

            board.push(row);
        }

        Game {
            board,
            rules,
        }
    }

    pub fn run(mut self, sleep_time: u64) -> Result<()> {
        self.setup()?;

        let mut now = SystemTime::now();
        
        loop {
            self.draw(now)?;
            now = SystemTime::now();
            self.update();
            thread::sleep(time::Duration::from_millis(sleep_time));
        }
    }

    pub fn randomize_board(mut self, count: u32) -> Self {
        let mut rng = rand::thread_rng();

        let y_max = self.board.len();
        let x_max = self.board[0].len();

        for _ in 0..count {
            let x = rng.gen_range(0..x_max);
            let y = rng.gen_range(0..y_max);

            self.board[y][x] = Cell::Alive;
        }

        self
    }

    fn setup(&self) -> Result<()> {
        let mut stdout = stdout();

        stdout.execute(cursor::Hide)?;
        stdout.execute(terminal::Clear(terminal::ClearType::All))?;

        Ok(())
    }

    fn draw(&self, deltatime: SystemTime) -> Result<()> {
        let mut stdout = stdout();

        for y in 0..self.board.len() {
            let row = &self.board[y];

            for x in 0..row.len()  {
                let cell = self.board[y][x];

                match cell {
                    Cell::Dead => stdout.queue(cursor::MoveTo(x as u16 + 1, y as u16  + 1))?.queue(style::PrintStyledContent("█".black()))?,
                    Cell::Alive => stdout.queue(cursor::MoveTo(x as u16  + 1, y as u16  + 1))?.queue(style::PrintStyledContent("█".green()))?,
                    Cell::OnceLifed => stdout.queue(cursor::MoveTo(x as u16  + 1, y as u16  + 1))?.queue(style::PrintStyledContent("█".blue()))?,
                };
            }
        }

        self.draw_frame(&mut stdout)?;

        let deltatime = deltatime.elapsed().unwrap().as_millis();

        stdout
            .queue(cursor::MoveTo(0, self.board.len() as u16 + 2))?
            .queue(style::Print(format!("Deltatime: {} ", deltatime)))?;

        stdout.flush()?;

        Ok(())
    }

    fn draw_frame(&self, stdout: &mut std::io::Stdout) -> Result<()> {
        for y in 0..self.board.len() + 2 {
            let row = &self.board[0];

            for x in 0..row.len() + 2 {
                if (y == 0 || y == (self.board.len() + 1)) || (x == 0 || x == row.len() + 1) {
                    stdout
                        .queue(cursor::MoveTo(x as u16,y as u16))?
                        .queue(style::PrintStyledContent("█".magenta()))?;
                }
            }
        }

        Ok(())
    }

    fn update(&mut self) {
        let mut board_copy = self.board.clone();

        for y in 0..self.board.len() {
            let row = &self.board[y];

            for x in 0..row.len() {
                let cell = self.board[y][x];

                let new_cell = self.rules.apply(&self.board, x as u16, y as u16, cell);
                board_copy[y][x] = new_cell;
            }
        }

        self.board = board_copy;
    }
}

#[derive(Copy, Clone)]
pub enum Cell {
    Dead,
    Alive,
    OnceLifed,
}

