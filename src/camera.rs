
use crate::game::Cell;
use std::io::stdout;
use std::time::SystemTime;
use crossterm::{
    ExecutableCommand, QueueableCommand,
    terminal, cursor, style::{self, Stylize}, Result
};

pub struct Camera {
    width: u8,
    height: u8,
    x: u32,
    y: u32,
}

impl Camera {
    pub fn new(width: u8, height: u8) -> Camera {
        Camera {
            x: 0,
            y: 0,
            width,
            height,
        }
    }

    fn draw(&self, board: &Vec<Vec<Cell>>) -> Result<()> {
        let mut stdout = stdout();

        for y in 0..board.len() {
            let row = &board[y];

            for x in 0..row.len()  {
                let cell = board[y][x];

                match cell {
                    Cell::Dead => stdout.queue(cursor::MoveTo(x as u16 + 1, y as u16  + 1))?.queue(style::PrintStyledContent("█".black()))?,
                    Cell::Alive => stdout.queue(cursor::MoveTo(x as u16  + 1, y as u16  + 1))?.queue(style::PrintStyledContent("█".green()))?,
                    Cell::OnceLifed => stdout.queue(cursor::MoveTo(x as u16  + 1, y as u16  + 1))?.queue(style::PrintStyledContent("█".blue()))?,
                };
            }
        }

        Ok(())
    }
}