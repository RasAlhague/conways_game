
use crate::game::Cell;
use crate::rules::Rules;

pub struct WrapingWorldRules;

impl Rules for WrapingWorldRules {
    fn apply(&self, board: &Vec<Vec<Cell>>, x: u16, y: u16, cell: Cell) -> Cell {
        let neighbours = get_neighbours(board, x, y);

        match cell {
            Cell::Dead | Cell::OnceLifed => {
                if neighbours == 3 {
                    return Cell::Alive;
                }

                return cell;
            },
            Cell::Alive => {
                if neighbours < 2 || neighbours > 3 {
                    return Cell::OnceLifed;
                }

                return cell;
            },
        }
    }
}

pub struct DynamicWrappingRules {
    revive: u8,
    overpopulated: u8,
    underpopulated: u8,
}

impl DynamicWrappingRules {
    pub fn new(revive: u8, overpopulated: u8, underpopulated: u8) -> Self {
        DynamicWrappingRules {
            revive,
            overpopulated,
            underpopulated
        }
    }
}

impl Rules for DynamicWrappingRules {
    fn apply(&self, board: &Vec<Vec<Cell>>, x: u16, y: u16, cell: Cell) -> Cell {
        let neighbours = get_neighbours(board, x, y);

        match cell {
            Cell::Dead | Cell::OnceLifed => {
                if neighbours == self.revive as u8 {
                    return Cell::Alive;
                }

                return cell;
            },
            Cell::Alive => {
                if neighbours < self.underpopulated || neighbours > self.overpopulated {
                    return Cell::OnceLifed;
                }

                return cell;
            },
        }
    }
}

fn get_neighbours(board: &Vec<Vec<Cell>>, x: u16, y: u16) -> u8 {
    let mut alive: u8 = 0;

    alive += is_alive_rel(board, x, y, 1, 1);
    alive += is_alive_rel(board, x, y, 0, 1);
    alive += is_alive_rel(board, x, y, -1, 1);
    alive += is_alive_rel(board, x, y, 1, 0);
    alive += is_alive_rel(board, x, y, -1, 0);
    alive += is_alive_rel(board, x, y, 1, -1);
    alive += is_alive_rel(board, x, y, 0, -1);
    alive += is_alive_rel(board, x, y, -1, -1);

    alive
}

fn is_alive_rel(board: &Vec<Vec<Cell>>, x: u16, y: u16, rel_x: i16, rel_y: i16) -> u8 {
    let mut y_val = y as i16 + rel_y;
    let mut x_val = x as i16 + rel_x;

    if y_val >= board.len() as i16  {
        y_val = 0;
    }

    if  y_val < 0  {
        y_val = (board.len() - 1) as i16;
    }

    if  x_val >= board[0].len() as i16  {
        x_val = 0;
    }

    if  x_val < 0 {
        x_val = (board[0].len() - 1) as i16;
    }

    if let Cell::Alive = board[y_val as usize][x_val as usize] {
        return 1;
    } 

    0
}