use crate::game::Cell;

pub trait Rules {
    fn apply(&self, board: &Vec<Vec<Cell>>, x: u16, y: u16, cell: Cell) -> Cell;
}

pub struct DefaultRules;

impl Rules for DefaultRules {
    fn apply(&self, board: &Vec<Vec<Cell>>, x: u16, y: u16, cell: Cell) -> Cell {
        let neighbours = get_neighbours(board, x, y);

        match cell {
            Cell::Dead | Cell::OnceLifed => {
                if neighbours == 3 {
                    return Cell::Alive;
                }

                return cell;
            },
            Cell::Alive => {
                if neighbours < 2 || neighbours > 3 {
                    return Cell::OnceLifed;
                }

                return cell;
            },
        }
    }
}

fn get_neighbours(board: &Vec<Vec<Cell>>, x: u16, y: u16) -> u8 {
    let mut alive: u8 = 0;

    alive += is_alive_rel(board, x, y, 1, 1);
    alive += is_alive_rel(board, x, y, 0, 1);
    alive += is_alive_rel(board, x, y, -1, 1);
    alive += is_alive_rel(board, x, y, 1, 0);
    alive += is_alive_rel(board, x, y, -1, 0);
    alive += is_alive_rel(board, x, y, 1, -1);
    alive += is_alive_rel(board, x, y, 0, -1);
    alive += is_alive_rel(board, x, y, -1, -1);

    alive
}

fn is_alive_rel(board: &Vec<Vec<Cell>>, x: u16, y: u16, rel_x: i16, rel_y: i16) -> u8 {
    let y_val = y as i16 + rel_y;
    let x_val = x as i16 + rel_x;

    if y_val >= board.len() as i16 || y_val < 0 || x_val >= board[0].len() as i16 || x_val < 0 {
        return 0;
    }

    if let Cell::Alive = board[y_val as usize][x_val as usize] {
        return 1;
    } 

    0
}
