mod game;
mod rules;
mod wrapping_rules;
mod camera;

use crate::wrapping_rules::DynamicWrappingRules;
use crate::rules::Rules;
use crate::wrapping_rules::WrapingWorldRules;
use crate::rules::DefaultRules;
use crate::game::Game;
use structopt::StructOpt;
use crossterm::Result;

fn main() -> Result<()> {
    let config = Config::from_args();

    let rules: Box<dyn Rules> = match config.rules {
        RuleSelect::Default => Box::new(DefaultRules),
        RuleSelect::Wrapping => Box::new(WrapingWorldRules),
        RuleSelect::DynWrapping { revive, overpopulated, underpopulated } => Box::new(DynamicWrappingRules::new(revive, overpopulated, underpopulated)),
    };

    let game = Game::new(config.width, config.height, rules)
        .randomize_board(config.rand);

    let mut sleeptime = 0;

    if config.fps > 0 {
        sleeptime = 1000/config.fps;
    }

    game.run(sleeptime)?;

    Ok(())
}

#[derive(Debug, StructOpt)]
#[structopt(name = "conways game of life", about = "Config for the game")]
struct Config {
    #[structopt(short = "f", long = "fps", default_value = "1")]
    fps: u64,
    #[structopt(short = "w", long = "width", default_value = "40")]
    width: u16,
    #[structopt(short = "h", long = "height", default_value = "20")]
    height: u16,
    #[structopt(short = "r", long = "rand", default_value = "50")]
    rand: u32,
    #[structopt(subcommand)]
    rules: RuleSelect,
}

#[derive(Debug, StructOpt)]
enum RuleSelect {
    Default,
    Wrapping,
    DynWrapping {
        #[structopt(short = "r", long = "revive")]
        revive: u8,
        #[structopt(short = "o", long = "overpopulated")]
        overpopulated: u8,
        #[structopt(short = "u", long = "underpopulated")]
        underpopulated: u8,
    },
}